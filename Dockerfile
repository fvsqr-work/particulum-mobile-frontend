FROM nginx:1.11-alpine

COPY ./write-config.sh /run.sh
COPY ./js.site.conf /etc/nginx/conf.d/default.conf

RUN chmod a+x /run.sh

CMD /run.sh

#-------------------------------------------------------------------------------
# App sources
#-------------------------------------------------------------------------------
COPY ./app /app

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
ARG DOCKER_IMAGE

LABEL com.roguewave.particulummobile.build-date=$BUILD_DATE \
      com.roguewave.particulummobile.name="$DOCKER_IMAGE" \
      com.roguewave.particulummobile.description="Particulum Mobile Frontend App (JS)" \
      com.roguewave.particulummobile.vcs-ref=$VCS_REF \
      com.roguewave.particulummobile.vcs-url="$VCS_URL" \
      com.roguewave.particulummobile.version=$VERSION \
      com.roguewave.particulummobile.schema-version="1.0"
