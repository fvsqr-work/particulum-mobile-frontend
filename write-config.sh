#!/bin/sh

echo "var BACKEND = '$BACKEND';" > /app/js/config.js
echo "var SWARM_MODE = $SWARM_MODE;" >> /app/js/config.js
echo "var CONTAINER_INFO = '$CONTAINER_INFO';" >> /app/js/config.js
echo "var SWARM_STACK_NAMESPACE = '$SWARM_STACK_NAMESPACE';" >> /app/js/config.js

nginx -g "daemon off;"
